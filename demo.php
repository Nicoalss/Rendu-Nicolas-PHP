<?php

$liens = [];
$images = [];
$noms = [];

if (isset($_POST['searchForm'])) {

  //Srapping imdb page
  $src = file_get_contents('https://www.imdb.com/find?q=' . urlencode($_POST['search']));

  preg_match('/<div class="findSection">(.|\n)*?<\/div>/i', $src, $match);

  preg_match_all('/<tr [^>]*>[\s]*<[^>]*>[\s]*<a href="([^"]*)" >[\s]*<img src="([^"]*)" \/>[\s]*<[^>]*>[\s]*<[^>]*>[\s]*<[^>]*>[\s]*<[^>]*>([^<]*)/i', $match[0], $films);

  $liens = $films[1];
  $images = $films[2];
  $noms = $films[3];
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Scrapper</title>
  </head>
  <body>

    <form action="#" method="post">
      <input type="text" name="search" value="">
      <button name="searchForm" type="submit" name="button">Rechercher</button>
    </form>

    <?php for ($i = 0; $i < count($liens); $i++): ?>
      <div class="">
        <img src="<?php echo $images[$i] ?>" alt="">
        <a href="demo-film.php?url=<?php echo urlencode($liens[$i]) ?>"><?php echo $noms[$i] ?></a>
      </div>
    <?php endfor; ?>

  </body>
</html>
