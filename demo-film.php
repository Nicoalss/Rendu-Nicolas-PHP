<?php

if (isset($_GET['url'])) {

  $src = file_get_contents('https://www.imdb.com' . $_GET['url']);

  preg_match('/<meta name="title" content="([^(]*) \([^\d]*([^)]*)/i', $src, $filmInfo);
  $name = $filmInfo[1];
  $year = $filmInfo[2];

  preg_match('/<div class="poster">[\s]*<[^>]*>[\s]*<img alt="[^"]*"[\s]*title="[^"]*"[\s]*src="([^"]*)/i', $src, $match);
  $cover = $match[1];

  preg_match('/[\s]*"ratingValue":(.*?)\n/i', $src, $match);
  $note = $match[1];
  $note = str_replace('"', '', $note);


  preg_match_all('/<tr [^>]*>[^>]*>[^>]*>[^<]*<img[^a]*alt="([^"]*)/i', $src, $matchs);
  $actors = $matchs[1];

}else {
  header('Location: demo.php');
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title></title>
  </head>
  <body class="container-fluid">
    <h1><?php echo $name . ' (' . $year . ')' ?></h1>
    <h3>Note : <?php echo $note ?> / 10</h3>
    <img src="<?php echo $cover ?>" alt="">
    <h4 class="mt-3">Acteurs :</h4>
    <?php foreach ($actors as $key => $actor): ?>
      <div class="">
        <span><?php echo $actor ?></span>
      </div>
    <?php endforeach; ?>
  </body>
</html>
